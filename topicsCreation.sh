# Use KAFKA_BROKER_URL environment variable, default to localhost if not set
BROKER_URL=${KAFKA_BROKER_URL:-localhost:9092}

echo "delete RawTweets topic if it exists"
$KAFKA_HOME/bin/kafka-topics.sh --delete --if-exists --bootstrap-server $BROKER_URL --topic RawTweets
echo "create RawTweets topic"
$KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server $BROKER_URL --partitions 5 --config retention.ms=3600000 --topic RawTweets

echo "delete FilteredTweets topic if it exists"
$KAFKA_HOME/bin/kafka-topics.sh --delete --if-exists --bootstrap-server $BROKER_URL --topic FilteredTweets
echo "create FilteredTweets topic"
$KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server $BROKER_URL --partitions 4 --config retention.ms=3600000 --topic FilteredTweets

echo "delete ExtractedHashtags topic if it exists"
$KAFKA_HOME/bin/kafka-topics.sh --delete --if-exists --bootstrap-server $BROKER_URL --topic ExtractedHashtags
echo "create ExtractedHashtags topic"
$KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server $BROKER_URL --partitions 3 --config retention.ms=3600000 --topic ExtractedHashtags

echo "delete TopHashtags topic if it exists"
$KAFKA_HOME/bin/kafka-topics.sh --delete --if-exists --bootstrap-server $BROKER_URL --topic TopHashtags
echo "create TopHashtags topic"
$KAFKA_HOME/bin/kafka-topics.sh --create --bootstrap-server $BROKER_URL --partitions 1 --config retention.ms=3600000 --topic TopHashtags
