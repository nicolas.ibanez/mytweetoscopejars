mkdir TestBases

cd TestBases

wget https://gitlab-research.centralesupelec.fr/galtier/3MD1510_IAL/-/raw/main/Tweetoscope/TestBases/miniTestBase.txt

wget https://gitlab-research.centralesupelec.fr/galtier/3MD1510_IAL/-/raw/main/Tweetoscope/TestBases/largeTestBase.txt

wget https://gitlab-research.centralesupelec.fr/galtier/3MD1510_IAL/-/raw/main/Tweetoscope/TestBases/scenarioTestBase.txt

